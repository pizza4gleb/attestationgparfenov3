@smoke
@krokodil
Feature: Wikipedia has Krokodil page

  Background: I am on the main page
    Given I am on the "https://wikipedia.org"

  Scenario: We can look for Krokodil page(TASK-1)
    Given "searchQuery" is set "Krokodil"
     When I fill "searchField" with "searchQuery" value
      And I click the "searchButton"
     Then "mainHeader" text is "Krokodil"

   Scenario: We can look for Crocodile page(TASK-2)
     Given "searchQuery2" is set "Crocodile"
      When I fill "searchField" with "searchQuery2" value
       And I click the "searchButton"
      Then "mainHeader" text is "Crocodile"

    Scenario: We can look for Krokodil page(TASK-1)
      Given "searchQuery3" is set "Krokodil"
       When I fill "searchField" with "searchQuery3" value
        And I click the "searchButton"
       Then "mainHeader" text is "Krokodil"

     Scenario: We can look for Crocodile page(TASK-2)
       Given "searchQuery4" is set "Crocodile"
        When I fill "searchField" with "searchQuery4" value
         And I click the "searchButton"
        Then "mainHeader" text is "Crocodile"