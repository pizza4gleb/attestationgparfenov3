package e2e.steps;

import e2e.data.DataProvider;
import e2e.pages.PageProvider;
import e2e.pages.WikiIndexPage;
import e2e.support.CucumberData;
import e2e.support.CucumberHelper;
import io.cucumber.java.AfterStep;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static com.codeborne.selenide.Selenide.open;
import static org.junit.jupiter.api.Assertions.*;

public class CommonSteps {
    public String var;

    @Given("{string} var")
    public void var(String string) throws Exception {
        var = (String) DataProvider.get(string);
    }

    @When("I look at the var")
    public void i_look_at_the_var() {
        assert(var != null);
    }

    @Then("the var is {string}")
    public void the_var_is(String string) throws Exception {
        assertEquals(var, DataProvider.get(string));
    }

    @Then("the var is not {string}")
    public void the_var_is_not(String string) throws Exception {
        assertNotEquals(var, DataProvider.get(string));
    }

    @Given("{string} is set {string}")
    public void is_set(String key, String value) throws Exception {
        DataProvider.set(key, value);
    }

    @Given("I am on the {string}")
    public void i_am_on_the(String url) {
        //CucumberData.getDriver().get(url);
        open(url);
    }

    @When("I fill {string} with {string} value")
    public void i_fill_with_value(String target, String value) throws Exception {
        CucumberData.getPage().find(target).sendKeys((String) DataProvider.get(value));
    }

    @When("I click the {string}")
    public void i_click_the(String target) {
        CucumberData.getPage().find(target).click();
    }

    @Then("{string} text is {string}")
    public void text_is_value(String target, String value) {
        assertEquals(value, CucumberData.getPage().find(target).getText());
    }


}
