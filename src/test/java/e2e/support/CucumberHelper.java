package e2e.support;

import e2e.data.DataProvider;
import e2e.pages.BasePage;
import e2e.pages.PageProvider;
import io.cucumber.java.*;
import org.junit.jupiter.api.AfterAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.xml.crypto.Data;
import java.time.Duration;

public class CucumberHelper {

    @Before("not @no-browser")
    public void tearUp(){
        CucumberData.setDriver(new ChromeDriver());
        CucumberData.getDriver().manage().timeouts().pageLoadTimeout(Duration.ofSeconds(10));
        CucumberData.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @After("not @no-browser")
    public void tearDown(){
        CucumberData.getDriver().quit();
    }

    @After
    public void bye(){
        System.out.println("Bye!");
    }



//    @BeforeAll
//    @AfterAll

    @BeforeStep
    @AfterStep
    public void screen(){

    }

    @AfterStep("not @no-browser")
    public void url() throws Exception {
        CucumberData.setProvider(new PageProvider(CucumberData.getDriver()));
        CucumberData.setPage(CucumberData.getProvider().find());
    }
}
