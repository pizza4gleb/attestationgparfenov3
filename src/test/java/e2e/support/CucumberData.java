package e2e.support;

import e2e.pages.BasePage;
import e2e.pages.PageProvider;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;

public class CucumberData {
    public static HashMap<Long, WebDriver> drivers = new HashMap<>();
    public static HashMap<Long, BasePage> pages = new HashMap<>();
    public static HashMap<Long, PageProvider> providers = new HashMap<>();

    public static Long id() {
        return Thread.currentThread().getId();
    }

    public static void setDriver(WebDriver driver) {
        drivers.put(id(), driver);
    }

    public static WebDriver getDriver() {
        return drivers.get(id());
    }

    public static void setPage(BasePage page) {
        pages.put(id(), page);
    }

    public static BasePage getPage() {
        return pages.get(id());
    }

    public static void setProvider(PageProvider provider) {
        providers.put(id(), provider);
    }

    public static PageProvider getProvider() {
        return providers.get(id());
    }



}
