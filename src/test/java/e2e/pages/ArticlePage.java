package e2e.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ArticlePage extends BasePage{

    private WebDriver d;
    public ArticlePage(WebDriver d) {
        super(d);
        one.put("mainHeader",
                By.cssSelector(
                        "#firstHeading"
                ));
    }
}
