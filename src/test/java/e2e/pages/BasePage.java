package e2e.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.List;

public class BasePage {
    public HashMap<String, By> one = new HashMap<>();
    public HashMap<String, By> many = new HashMap<>();
    private WebDriver d;

    BasePage(WebDriver d) {
        this.d = d;
    }

    public SelenideElement find(String name) {
        SelenideElement xxx = Selenide.$(one.get(name));
        if (xxx.exists()) {
            return xxx;
        }
        return Selenide.$(name);
        //return d.findElement(one.get(name));
    }

    public List<WebElement> all(String name) {
        return d.findElements(one.get(name));
    }
}
