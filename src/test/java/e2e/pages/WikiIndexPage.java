package e2e.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class WikiIndexPage extends BasePage{
    public WikiIndexPage(WebDriver d) {
        super(d);
        one.put("searchField",
                        By.cssSelector(
                                "#searchInput"
                        ));
        one.put("searchButton",
                        By.cssSelector(
                                "button[type=submit]"
                        ));
    }


}
