package e2e.pages;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;

public class PageProvider {
    public HashMap<String, BasePage> pages = new HashMap<>();
    public WebDriver driver;

    public PageProvider(WebDriver driver) {
        this.driver = driver;
        pages.put("https://en.wikipedia.org/wiki/[a-zA-Z0-9]*$",
                  new ArticlePage(driver));
        pages.put("https://www.wikipedia.org/",
                  new WikiIndexPage(driver));
    }



    public BasePage find() throws Exception {
        String url = Selenide.webdriver().object().getCurrentUrl();
        for (String key : pages.keySet()) {
            if (url.matches(key)) {
                return pages.get(key);
            }
        }
        return new BasePage(driver);
    }
}
